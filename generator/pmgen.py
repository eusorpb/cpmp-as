#!/usr/bin/python

#
# This code is Copyright (c) 2014 Kevin Tierney and is released under a BSD-new
# license (see license.txt)
#

import argparse
import copy
import random
import sys

# Generates a Caserta-Voss like instance. For the given number of tiers and
# stacks, every cell is filled with a container with a unique group ID. The
# containers are permuted randomly.
def genCV(stacks, tiers):
    containers = range(1, (stacks * tiers) + 1)
    random.shuffle(containers)
    ret = []
    for ss in xrange(stacks):
        start_pos = ss*tiers
        end_pos = (ss + 1) * tiers
        ret.append(containers[start_pos:end_pos])
    return ret

# Attempts to generate a BF-like instance. They don't completely describe their
# algorithm, so getting this exactly right is just about impossible.
def genBF(nstacks, ntiers, ncontainers, ngroups, nbad):
    stacks = [[] for ss in xrange(nstacks)]
    not_full_stacks = range(nstacks)
    overstowed_stacks = {ii : False for ii in xrange(nstacks)}
    badly_placed = 0
    cleft = ncontainers
    while cleft > 0:
        if badly_placed < nbad:
            ss = random.choice(not_full_stacks)
            if len(stacks[ss]) == 0 and random.random() < 0.8:
                ss = random.choice(not_full_stacks)
            stacks[ss].append(random.randint(1, ngroups))
        else:
            good_stacks = [ss for ss in xrange(nstacks) if not overstowed_stacks[ss] and ss not in not_full_stacks]
            if len(good_stacks) > 0:
                ss = random.choice(good_stacks)
            else:
                # Perform a repair here. The idea would be to offset the extra overstow by trying to sort a stack better.
                overstow_point = -1
                nfs_shuffle = copy.deepcopy(not_full_stacks)
                random.shuffle(nfs_shuffle)
                ss = 0
                for ss in nfs_shuffle:
                    for tt in xrange(0, len(stacks[ss]) - 1):
                        if stacks[ss][tt] < stacks[ss][tt + 1]:
                            overstow_point = tt
                            break
                for tt in xrange(overstow_point + 1, len(stacks[ss]) - 1):
                    if stacks[ss][tt] <= stacks[ss][overstow_point]:
                        stacks[ss][tt], stacks[ss][overstow_point] = stacks[ss][overstow_point], stacks[ss][tt]
#                     return genBF(nstacks, ntiers, ncontainers, ngroups, nbad) # we failed. give up and try again
#                 else:
#                     print "Error generating instance: No more good stacks available, but badly placed containers are already all assigned."
#                     print nstacks, ntiers, ncontainers, ngroups, nbad
#                     return []
            if len(stacks[ss]) >= 1:
                stacks[ss].append(random.randint(1, stacks[ss][-1]))
            else:
                stacks[ss].append(random.randint(1, ngroups))
        if len(stacks[ss]) >= 2 and stacks[ss][-1] > stacks[ss][-2]:
            overstowed_stacks[ss] = True
        if overstowed_stacks[ss]:
            badly_placed += 1
        if len(stacks[ss]) == ntiers:
            not_full_stacks.remove(ss)
        cleft -= 1
    return stacks

# Outputs an instance in the Caserta-Voss format. This accepts an instance as a
# list of lists of containers, where each list is a stack from bottom to top.
# output_loc refers to the file path to output to, None for standard out
def outputCV(inst, inst_id, output_loc):
    stacks = len(inst)
    tiers = max([len(ss) for ss in inst])
    containers = sum([len(ss) for ss in inst])
    # TODO There must be a correct way of doing this (ignoring try/except for now)
    fp = sys.stdout
    if output_loc:
        fp = open("{0}-{1}.dat".format(output_loc, inst_id), 'w')
    fp.write("{0} {1}\n".format(stacks, containers))
    for ss in inst:
        stack_out = " ".join(map(str, ss))
        fp.write("{0} {1}\n".format(len(ss), stack_out))
    if output_loc:
        fp.close()

def outputBF(inst, label, stacks, tiers, containers, inst_id, output_loc):
    fp = sys.stdout
    if output_loc:
        fp = open("{0}{1}-{2}.bay".format(output_loc, label, inst_id), 'w')
    fp.write("Label : {0}\n".format(label))
    fp.write("Width : {0}\n".format(stacks))
    fp.write("Height : {0}\n".format(tiers))
    fp.write("Containers : {0}\n".format(containers))
    for ii, ss in enumerate(inst):
        fp.write("Stack {0} : {1}\n".format(ii, " ".join(map(str, ss))))
    if output_loc:
        fp.close()

if __name__ == "__main__":
    class Args:
        pass
    parser = argparse.ArgumentParser(description='Pre-marshalling problem instance generator.')
    parser.add_argument('--seed', type=int, default=-1, help='Random seed initialization. -1 indicates the default python seed should be used.')
    parser.add_argument('--cvbf', type=str, default="CV", help='CV for CV instances, BF for BF-like instances.')
    parser.add_argument('--prefix', type=str, default=None, help='Output prefix for the instances.')
    parser.add_argument('--num', type=int, default=1, help='Number of instances to produce.')
    parser.add_argument('--bfcontainerspct', type=float, default=0.6, help='Percentage of slots to fill with conatiners (BF only,[0,1]).')
    parser.add_argument('--bfgroupspct', type=float, default=0.2, help='Number of groups as a percentage of the number of containers (BF only, [0,1]).')
    parser.add_argument('--bfbadpct', type=float, default=0.6, help='Number of badly placed containers as a percentage of the number of containers (BF only, [0,1]).')
    parser.add_argument('stacks', type=int, default=3, help='Number of stacks in the problem.')
    parser.add_argument('tiers', type=int, default=3, help='Number of tiers in the problem.')

    args = Args()
    parser.parse_args(namespace=args)
    if args.seed > -1:
        random.seed(args.seed)
    
    for ii in xrange(1, args.num + 1):
        if args.cvbf.upper() == "CV":
            cvinst = genCV(args.stacks, args.tiers)
            outputCV(cvinst, ii, args.prefix)
        else:
            stacks = args.stacks
            tiers = args.tiers
            ncontainers = int(round(stacks * tiers * args.bfcontainerspct))
            ngroups = int(round(ncontainers * args.bfgroupspct))
            nbad = int(round(ncontainers * args.bfbadpct))
            bfinst = genBF(stacks, tiers, ncontainers, ngroups, nbad)
            label = "{0}_{1}_{2}_{3}_{4}".format(stacks, tiers, ncontainers, ngroups, nbad)
            outputBF(bfinst, label, stacks, tiers, ncontainers, ii, args.prefix)

        if not args.prefix and ii + 1 < args.num:
            print

    

