/*
 * This code is Copyright (c) 2014 Kevin Tierney and is licensed under the
 * BSD-new license. See license.txt for details.
 */

/*
 * Class: ResourceManager
 * Description: Manages resources assigned to the program, such as memory and
 * (more abstractly) CPU time, ensuring the program does not exceed any hard
 * limits. Singleton.
 */

#ifndef RESOURCE_MANAGER
#define RESOURCE_MANAGER

#include <sys/resource.h>
#include <ctime>

class ResourceManager {
private:
    static ResourceManager* s_resourceManager;
    ResourceManager();
public:
    static bool s_cpuLimitNotReached;
    static time_t s_initialTime;
    static time_t s_maxWallClock;
    static ResourceManager& getResourceManager();
    static inline bool cpuLimitNotReached() {
        return s_cpuLimitNotReached;
    }
    static void sigxCpuSignalHandler(int sig);
    static void sigIntTermHandler(int sig);

public:
    ~ResourceManager();

    /*
     * Sets the maximum allowed amount of memory to maxMemory bytes. When the
     * memory limit is reached, an ENOMEM error is generated.
     */
    void setMemoryLimit(long long maxMemory);
    
    /*
     * Sets the CPU user time limit. softLimit <= hardLimit. When the softLimit
     * is reached, a SIGXCPU signal is sent to the program as a warning, and
     * cpuLimitReached() will be set to true. When the hardLimit is reached, the
     * program is killed. It is advised to set the hardLimit so something like
     * softLimit + 5, and use the softLimit to clean up/exit.
     */
    void setCpuLimit(double softLimit, double hardLimit);

    long long memoryLimit() const;
    bool memoryLimitSet() const;
    double cpuSoftLimit() const;
    double cpuHardLimit() const;
    bool cpuLimitSet() const;

    inline static double userTime() {
        struct rusage ru;
        getrusage(RUSAGE_SELF, &ru);
        struct timeval tim;
        tim = ru.ru_utime;
        double cputime = (double)tim.tv_sec + ((double)tim.tv_usec / (double)CLOCKS_PER_SEC);
        tim = ru.ru_stime;
        return cputime + (double)tim.tv_sec + ((double)tim.tv_usec / (double)CLOCKS_PER_SEC);
    }
    static time_t wallClockTime();
    static time_t passedWallClockTime();
private:
    long long m_memLimit;
    bool m_memLimitSet;
    double m_cpuSoftLimit;
    double m_cpuHardLimit;
    bool m_cpuLimitSet;
};

#endif
