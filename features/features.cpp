/*
 * Computes features for the pre-marshalling problem.
 * Supported file formats:
 *  - Caserta & Voss
 *  - Bortfeldt & Forster
 *  - Exposito et al.
 *
 * The output of this program is to append the following lines to the following files (creating them if necessary):
 * - feature_values.arff: the features for the given instance
 * - feature_runstatus.arff: which features were successfully created
 * - feature_costs.arff: the processing time of each feature group
 *
 *
 * This code is Copyright (c) 2014 Kevin Tierney and is licensed under the
 * BSD-new license. See license.txt for details.
 *
 */

#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <limits>
#include <algorithm>

#include "Instance.h"
#include "ResourceManager.h"

void printHelp();
void basicFeats(const Instance& inst, std::vector<double>& feats, std::vector<std::string>& names);
void lbFeats(const Instance& inst, std::vector<double>& feats, std::vector<std::string>& names);
void stackFeats(const Instance& inst, std::vector<double>& feats, std::vector<std::string>& names);
void miscFeats(const Instance& inst, std::vector<double>& feats, std::vector<std::string>& names);
int emptyCellRecurse(ushort tt, ushort ss, const Instance& inst, std::vector<std::vector<bool> >& cell_counted, bool& has_empty);
void outputFeatValues(std::string& instid, const std::vector<double>& feats, const std::vector<std::string>& names);
void outputFeaturesHelper(std::ostream& out, std::string& instid, const std::vector<double>& feats);
void outputFeatRunStatus(std::string& instid, const std::vector<std::string>& names);
void minMaxMeanStd(const std::vector<int>& values, int& vmin, int& vmax, double& vmean, double& vstd);
bool fileExists(const char* fpath);

const char* VALUES_FILE = "feature_values.arff";
const char* COSTS_FILE = "feature_values.arff";
const char* RSTATUS_FILE = "feature_runstatus.arff";

int main(int argc, char** argv) {
    if(argc < 2 || argc > 4) {
        printHelp();
        return 1;
    }
    std::string fpath(argv[1]);
    if(fpath == "-h" || fpath == "--help") {
        printHelp();
        return 1;
    }
    bool outputStdOut = false;
    std::string instid;
    if(argc >= 3) {
        outputStdOut = std::string(argv[2]) == "-print";
        if(argc == 3 && outputStdOut) {
            instid.assign(argv[1]);
        } else {
            instid.assign(argv[argc - 1]);
        }
    } else {
        instid.assign(argv[1]);
    }

    
    Instance inst(fpath.c_str());
    std::vector<double> feats;
    std::vector<std::string> names;
    double startUtime = ResourceManager::userTime();
    basicFeats(inst, feats, names);
    lbFeats(inst, feats, names);
    stackFeats(inst, feats, names);
    miscFeats(inst, feats, names);
//     std::cout << "Feature computation took " << (ResourceManager::userTime() - startUtime) << "s." << std::endl;

    if(outputStdOut) {
        outputFeaturesHelper(std::cout, instid, feats);
    } else {
        outputFeatValues(instid, feats, names);
        outputFeatRunStatus(instid, names);
    }

    return 0;
}

void printHelp() {
    std::cerr << "Usage: ./pmfeatures <instance file> [-print] [instance_id]" << std::endl;
    std::cerr << "\tinstance file: path to the pre-marshalling instance to analyze. Supported formats: Caserta & Voss, Bortfeldt & Forster, Exposito et al." << std::endl;
    std::cerr << "\t-print Outputs features to stdout and ignores feature run status information, otherwise features are placed into feature_values.arff and run status into feature_runstatus.arff." << std::endl;
    std::cerr << "\tinstance_id: optional name for the instance in the feature file; if omitted, the instance path given is used" << std::endl;
}

void basicFeats(const Instance& inst, std::vector<double>& feats, std::vector<std::string>& names) {
    names.push_back("stacks");
    feats.push_back(inst.stacks()); // FEAT: # stacks
    names.push_back("tiers");
    feats.push_back(inst.tiers()); // FEAT: # tiers
    names.push_back("stack-tier-ratio");
    feats.push_back(inst.tiers() / (double)inst.stacks()); // FEAT: tiers/stacks ratio

    std::vector<int> groupCounts(inst.latestPriority(), 0);
    std::vector<int> topGood;

    int usedCount = 0;
    int numEmptyStacks = 0;
    int stacksWithOverstowage = 0;
    int g2StacksWithOverstowage = 0; // stacks with at least 2 containers in them
    int g2Stacks = 0;
    for(short ss = 0; ss < inst.stacks(); ++ss) {
        bool overstowing = false;
        int containersInStack = 0;
        if(inst.atCell(0, ss) == 0) numEmptyStacks++;
        for(short tt = 0; tt < inst.tiers(); ++tt) {
            cell iac = inst.atCell(tt, ss);
            if(iac > 0) {
                usedCount++;
                groupCounts[iac - 1]++;
                containersInStack++;
            }
            bool overstowingNow = ((tt >= 1) && iac > inst.atCell(tt - 1, ss));
            if(!overstowing && overstowingNow) { // Overstowage border
                topGood.push_back(inst.atCell(tt - 1, ss));
            }
            overstowing = overstowing || overstowingNow;
        }
        if(containersInStack >= 2) g2Stacks++;
        if(overstowing) {
            stacksWithOverstowage++;
            if(containersInStack >= 2) g2StacksWithOverstowage++;
        }
    }
    int totalCells = inst.stacks() * inst.tiers();

    names.push_back("container-density");
    feats.push_back(usedCount / (double)totalCells); // FEAT: container density 
    names.push_back("empty-stack-pct");
    feats.push_back(numEmptyStacks / (double)inst.stacks()); // FEAT: empty stack ratio
    names.push_back("overstowing-stack-pct");
    feats.push_back(stacksWithOverstowage / (double)inst.stacks());
    names.push_back("overstowing-2cont-stack-pct"); // based on latent feature iteration 2; feature 0
    feats.push_back(g2StacksWithOverstowage / (double)g2Stacks);

    double groupMean = 0.0;
    int groupMin = inst.latestPriority();
    int groupMax = 0;
    double groupStd = 0.0;
    minMaxMeanStd(groupCounts, groupMin, groupMax, groupMean, groupStd);

    names.push_back("group-same-min");
    feats.push_back(groupMin);
    names.push_back("group-same-max");
    feats.push_back(groupMax);
    names.push_back("group-same-mean");
    feats.push_back(groupMean);
    names.push_back("group-same-stdev");
    feats.push_back(groupStd);

    double topMean = 0.0;
    int topMin = inst.latestPriority();
    int topMax = 0;
    double topStd = 0.0;
    if(topGood.size() == 0) topGood.push_back(0);
    minMaxMeanStd(topGood, topMin, topMax, topMean, topStd);

    names.push_back("top-good-min");
    feats.push_back(topMin);
    names.push_back("top-good-max");
    feats.push_back(topMax);
    names.push_back("top-good-mean");
    feats.push_back(topMean);
    names.push_back("top-good-stdev");
    feats.push_back(topStd);
}

void lbFeats(const Instance& inst, std::vector<double>& feats, std::vector<std::string>& names) {
    int overstowage = 0;
    // Computing lower bound from Borgfeldt & Forster 
    // badly placed container = overstowing container 

    std::vector<int> lbDemand(inst.latestPriority() + 1, 0);
    std::vector<int> lbSupply(inst.latestPriority() + 1, 0);
    std::vector<int> lbHighestWplaced(inst.stacks(), -1);
    std::vector<bool> lbStackHasOverstowage(inst.stacks(), false);
    short directMoves = 0;
    std::vector<bool> empty(inst.stacks(), false);

    // Compute n'_{BX}
    int stackOverstowed = 0;
    int minBadlyPlaced = std::numeric_limits<int>::max();
    int supplyValue = 0;
    for(short ss = 0; ss < inst.stacks(); ++ss) {
        bool afterHighestWp = false;
        bool overstowed = false;
        stackOverstowed = 0;
        const cell& bottom = inst.atCell(0, ss);
        if(bottom > 0) {
            lbHighestWplaced[ss] = bottom;
        }

        for(short tt = 1; tt < inst.tiers(); tt++) {
            const cell& current = inst.atCell(tt, ss);
            const cell& below = inst.atCell(tt - 1, ss);
            if(!overstowed && (below < current)) {
                supplyValue = below;
                lbHighestWplaced[ss] = below;
                overstowed = true;
                lbStackHasOverstowage[ss] = true;
                afterHighestWp = true;
            }
            if(!afterHighestWp && bottom > 0 && current == 0) {
                afterHighestWp = true;
                supplyValue = below;
            }
            if(afterHighestWp) {
                lbSupply[supplyValue]++;
            }
            if(overstowed) {
                if(current != 0) {
                    lbDemand[current]++;
                    stackOverstowed++;
                }
            } else if(current > 0) {
                lbHighestWplaced[ss] = current;
            }
        }
        overstowage += stackOverstowed;
        minBadlyPlaced = std::min(minBadlyPlaced, stackOverstowed);
    }
    directMoves = overstowage;
    names.push_back("overstowage-pct");
    feats.push_back(directMoves / (double)(inst.stacks() * inst.tiers())); // FEAT: pct overstowage
    overstowage += minBadlyPlaced; // ignore as feat; used in BF LB

    // Compute the maximum cumulative demand surplus g*
    // Note that D(g) is computed on the fly by accumulating the values in the demand vector
    int cmDemand = 0;
    int numEmptyStacks = 0;
    for(auto itr = empty.begin(); itr != empty.end(); ++itr) {
        numEmptyStacks += (*itr) ? 1 : 0;
    }
    int cmSupply = numEmptyStacks * inst.tiers();
    int maxCmDmdSup = 0;
    int gstar = 0;
    for(short gg = inst.latestPriority(); gg > 0; --gg) {
        cmDemand += lbDemand[gg];
        cmSupply += lbSupply[gg];
        int tmpCmDmdSup = cmDemand - cmSupply;
        if(tmpCmDmdSup > maxCmDmdSup) {
            maxCmDmdSup = tmpCmDmdSup;
            gstar = gg;
        }
    }
    int nsgx = std::max(0, (int)std::ceil(maxCmDmdSup / (double)inst.tiers()));
    std::vector<int> ngstar; // n^{g^*}(s) for all stacks in which the highest well placed item belongs to a group g < gstar
    ngstar.reserve(inst.stacks());

    for(size_t ss = 0; ss < inst.stacks(); ++ss) {
        if(lbHighestWplaced[ss] < gstar) {
            ngstar.push_back(0);
            if(!empty[ss]) {
                // iterate up stack ss until the current tier is overstowing
                for(size_t tt = 0; tt < inst.tiers() && inst.atCell(tt, ss) > 0 && (tt == 0 || inst.atCell(tt, ss) <= inst.atCell(tt - 1, ss)); tt++) {
                    if(inst.atCell(tt,ss) < gstar) ngstar.back()++;
                }
            }
        }
    }

    // Compute n'_{GX}; 
    std::sort(ngstar.begin(), ngstar.end());
//     assert((int)ngstar.size() >= nsgx);
    for(int ii = 0; ii < nsgx; ++ii) {
        overstowage += ngstar[ii];
    }

    names.push_back("bflb");
    feats.push_back(overstowage); // FEAT: BF lower bound
}

void stackFeats(const Instance& inst, std::vector<double>& feats, std::vector<std::string>& names) {
    // Weighted left lpct% density
    double lpct = 0.3; // TODO could parameterize this
    ushort lstacks = (ushort)std::ceil(inst.stacks() * lpct); // ceil to ensure at least one column is selected
    int empty = 0;
    int total_slots = 0;
    for(ushort ss = 0; ss < lstacks; ++ss) {
        int stack_weight = (lstacks - ss + 1);
        stack_weight *= stack_weight; // trying some scaling
        total_slots += stack_weight * inst.tiers();
        for(ushort tt = 0; tt < inst.tiers(); ++tt) {
            if(inst.atCell(tt, ss) == 0) {
                empty += stack_weight; // weight by how far left the empty slot is
                // Note: extending to entire problem didn't seem to help
            }
        }
    }
    double density = empty / (double)total_slots;
    names.push_back("left-density");
    feats.push_back(density);

    // Gravity
    int gravity = 0;
    int max_gravity = 0;
    for(ushort tt = 0; tt < inst.tiers(); ++tt) {
        int tier_weight = (inst.tiers() - tt + 1);
        tier_weight *= tier_weight;
        max_gravity += tier_weight * inst.latestPriority() * inst.stacks();
        for(ushort ss = 0; ss < inst.stacks(); ++ss) {
            gravity += tier_weight * inst.atCell(tt, ss);
        }
    }
    double fgravity = (gravity / (double)max_gravity);
    names.push_back("tier-weighted-groups");
    feats.push_back(fgravity);
}

void miscFeats(const Instance& inst, std::vector<double>& feats, std::vector<std::string>& names) {
    // (average) L1 distance of largest container group from top left
    int lgcount = 0;
    int l1sum = 0;
    for(ushort ss = 0; ss < inst.stacks(); ++ss) {
        for(ushort tt = 0; tt < inst.tiers(); ++tt) {
            if(inst.atCell(tt, ss) == inst.latestPriority()) {
                lgcount++;
                l1sum += (2 * (inst.tiers() - tt)) + ss;
            }
        }
    }
    double avgdist = l1sum / (double)lgcount;
    // Normalize feature by dividing by the weighted L1 diagonal
    avgdist /= (2 * inst.tiers() + inst.stacks());
    names.push_back("avg-l1-top-left-lg-group");
    feats.push_back(avgdist);

    // Sum of continguous empty space if at least one piece of the space is an empty stack
    int total_cont_empty = 0;
    std::vector<std::vector<bool> > cell_counted(inst.tiers(), std::vector<bool>(inst.stacks(), false));
    for(ushort ss = 0; ss < inst.stacks(); ++ss) {
        for(ushort tt = 0; tt < inst.tiers(); ++tt) {
            if(inst.atCell(tt, ss) == 0 && !cell_counted[tt][ss]) {
                bool has_empty_stack = false;
                int tmp_empty = emptyCellRecurse(tt, ss, inst, cell_counted, has_empty_stack);
//                 if(tmp_empty > inst.tiers() && has_empty_stack) total_cont_empty += tmp_empty;
                if(has_empty_stack) total_cont_empty += tmp_empty;
            }
        }
    }
    double cont_empt_feat = total_cont_empty / (double)(inst.tiers() * inst.stacks());
    names.push_back("cont-empty-grt-estack");
    feats.push_back(cont_empt_feat);
//     std::cout << "cont-empty " << cont_empt_feat << std::endl;

    // Percentage of low valued containers on top of stacks (TODO multiplied by density) (latent feature 4; iteration 2)
    int countLow = 0;
    int minLow = (int)(inst.latestPriority() / 4.0);
    int topTier = -1;
    bool useFeature = true;
    for(ushort ss = 0; ss < inst.stacks(); ++ss) {
        for(ushort tt = 0; tt < inst.tiers(); ++tt) {
            if((tt == inst.tiers() - 1 || inst.atCell(tt + 1, ss) == 0) && inst.atCell(tt, ss) > 0 && inst.atCell(tt, ss) < minLow) {
                countLow++;
                if(topTier == -1) {
                    topTier = tt;
                } else {
                    useFeature = useFeature && (topTier == tt);
                }
                break;
            }
        }
    }
//     std::cout << (countLow / (double)inst.stacks()) << std::endl;
    names.push_back("pct-bottom-pct-on-top");
    if(useFeature) {
        feats.push_back(countLow / (double)inst.stacks());
    } else {
        feats.push_back(0.0);
    }
}

// Counts the number of empty cells in a contiguous block
int emptyCellRecurse(ushort tt, ushort ss, const Instance& inst, std::vector<std::vector<bool> >& cell_counted, bool& has_empty) {
    if(cell_counted[tt][ss] || inst.atCell(tt, ss) > 0) return 0;
    if(inst.atCell(0, ss) == 0) has_empty = true;
    cell_counted[tt][ss] = true;
    int ret = 1;
    if(tt - 1 >= 0) ret += emptyCellRecurse(tt - 1, ss, inst, cell_counted, has_empty);
    if(tt + 1 < inst.tiers()) ret += emptyCellRecurse(tt + 1, ss, inst, cell_counted, has_empty);
    if(ss - 1 >= 0) ret += emptyCellRecurse(tt, ss - 1, inst, cell_counted, has_empty);
    if(ss + 1 < inst.stacks()) ret += emptyCellRecurse(tt, ss + 1, inst, cell_counted, has_empty);
    return ret;
}

void outputFeatValues(std::string& instid, const std::vector<double>& feats, const std::vector<std::string>& names) {
    bool outputHeader = !fileExists(VALUES_FILE);

    std::ofstream fout(VALUES_FILE, std::fstream::out | std::fstream::app);
    if(fout.is_open()) {
        if(outputHeader) {
            fout << "@RELATION feature_values_premarshalling_astar_2013" << std::endl << std::endl;
            fout << "@ATTRIBUTE instance_id STRING" << std::endl;
            for(auto itr = names.begin(); itr != names.end(); ++itr) {
                fout << "@ATTRIBUTE " << *itr << " NUMERIC" << std::endl;
            }
            fout << std::endl << "@DATA" << std::endl;
        }
        outputFeaturesHelper(fout, instid, feats);
        fout.close();
    }
}

void outputFeaturesHelper(std::ostream& out, std::string& instid, const std::vector<double>& feats) {
    out << instid << ",";
    for(auto itr = feats.begin(); itr != feats.end(); ++itr) {
        out << *itr;
        if(itr + 1 != feats.end()) {
            out << ",";
        }
    }
    out << std::endl;
}

// There is nothing that can go wrong with these features if the instance is
// well formed, and there isn't any error handling anyway, so just report
// everything was OK.
void outputFeatRunStatus(std::string& instid, const std::vector<std::string>&
        names) {
    bool outputHeader = !fileExists(RSTATUS_FILE);
    std::ofstream fout(RSTATUS_FILE, std::fstream::out | std::fstream::app);
    if(fout.is_open()) {
        if(outputHeader) {
            fout << "@RELATION feature_runstatus_premarshalling_astar_2013" << std::endl << std::endl;
            fout << "@ATTRIBUTE instance_id STRING" << std::endl;
            for(auto itr = names.begin(); itr != names.end(); ++itr) {
                fout << "@ATTRIBUTE " << *itr << " {ok, timeout, memout, presolved, crash, other}" << std::endl;
            }
            fout << "@ATTRIBUTE presolved {true,false}" << std::endl;
            fout << std::endl << "@DATA" << std::endl;
        }
        fout << instid << ",";
        for(auto itr = names.begin(); itr != names.end(); ++itr) {
            fout << "ok,";
        }
        fout << "false" << std::endl;
    }
}

// Computes the min, max, mean and standard deviation of values and puts the
// values into vmin, vmax, vmean and vstd, respectively. Note: vmin, vmax,
// vmean and vstd need to be initialized
void minMaxMeanStd(const std::vector<int>& values, int& vmin, int& vmax,
        double& vmean, double& vstd) {
    for(auto itr = values.begin(); itr != values.end(); ++itr) {
        vmean += *itr;
        vmin = std::min(vmin, *itr);
        vmax = std::max(vmax, *itr);
    }
    vmean /= values.size();
    for(auto itr = values.begin(); itr != values.end(); ++itr) {
        vstd += std::pow(*itr - vmean, 2);
    }
    vstd = std::sqrt(vstd / values.size());
}

bool fileExists(const char* fpath) {
    std::ifstream fin(fpath);
    if(fin.good()) {
        fin.close();
        return true;
    }
    return false;
}



