/*
 * This code is Copyright (c) 2014 Kevin Tierney and is licensed under the
 * BSD-new license. See license.txt for details.
 */

#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>

#include "Instance.h"

Instance::Instance(const char* instPath) 
        : m_tiers(0), m_stacks(0), m_latestPriority(0), m_cells() {

//     std::cout << "Loading instance: " << instPath << std::endl;
    std::ifstream input(instPath);
    if(input.is_open()) {
        std::string tmp;
        input >> tmp;
        if(tmp == "Label") { // BF instance
            loadInstanceBF(input);
        } else if(tmp == "Name:") { //Pre-Marshalling Problem: Heuristic solution method and instances generator, Christopher Expósito-Izquierdo , Belén Melián-Batista , Marcos Moreno-Vega
            loadInstanceExpo(input);
        } else {
            int dataFileFlag;
            std::stringstream ss;
            ss << tmp;
            ss >> dataFileFlag;
            if(dataFileFlag < 0) {
                loadInstanceDario(input);
            } else {
                loadInstanceCV(input, dataFileFlag);
            }
        }
    input.close();  
  } else {
      std::cerr << "Instance not found: " << instPath << std::endl;
      exit(1);
  }
}

void Instance::loadInstanceBF(std::ifstream& input) {
//     std::cout << "Loading BF formatted instance." << std::endl;
    std::string tmp;
    // chew through the label until we hit "Width"
    while((input >> tmp) && tmp != "Width"); // Note: This will fail if the word "Width" is in the label..
    input >> tmp; // colon
    input >> m_stacks;
    input >> tmp; input >> tmp;
    input >> m_tiers;
    input >> tmp; input >> tmp; input >> tmp; // ignore containers line
    m_cells.resize(m_tiers * m_stacks, 0);
    input >> tmp; // skip first "Stack"
    for(short ss = 0; ss < m_stacks; ++ss) {
        input >> tmp; input >> tmp; // Skip number and colon
        bool stackDone = false;
        for(short tt = 0; tt < m_tiers + 1 && !stackDone; ++tt) { // tiers + 1 because we need to read "Stack"
            stackDone = !(input >> tmp);
            cell slot = 0;
            if(!stackDone && tmp != "Stack") {
                std::stringstream sstr;
                sstr << tmp;
                sstr >> slot;
                atCell(tt, ss) = slot + 1;
                m_latestPriority = std::max((cell)m_latestPriority, atCell(tt, ss));
            } else {
                stackDone = true;
            }
        }
    }
}

void Instance::loadInstanceExpo(std::ifstream& input) {
//     std::cout << "Reading instance in Exposito paper format."<<std::endl;
    std::string tmp;
    while((input >> tmp) && tmp != "Tiers:");// Note: This will fail if the word "Tiers" is in the label..
    input >> m_tiers; 
    m_tiers += 2;//adding 2 empty tiers, needed to solve 100% instances, unclear if needed for 50 and 75%
    input >> tmp;//Stacks:
    input >> m_stacks;
    input >> tmp; input >> tmp;// ignore containers line
    m_cells.resize(m_stacks * m_tiers, 0);
    input >> tmp; //skip Stack
    for(int ss = 0; ss < m_stacks; ++ss) {
        input >> tmp; //skip Stack number and colon
        bool stackDone = false;
        for(short tt = 0; tt < m_tiers + 1 && !stackDone; ++tt) { // tiers + 1 because we need to read "Stack"
            stackDone = !(input >> tmp);
            short slot = 0;
            if(!stackDone && tmp != "Stack") {
                std::stringstream sstr;
                sstr << tmp;
                sstr >> slot;
                atCell(tt, ss) = slot + 1; // add 1 since 0 is valid in the instances
                m_latestPriority = std::max((cell)m_latestPriority, atCell(tt, ss));
            } else {
                stackDone = true;
            }
        }
    }
}

void Instance::loadInstanceDario(std::ifstream& input) {
//     std::cout << "Reading instance in Dario's format." << std::endl;
    // Read in our data format
    input >> m_stacks;
    input >> m_tiers;

    m_cells.resize(m_stacks * m_tiers, 0);

    // create bay
    for(short tt = m_tiers-1; tt >= 0;tt--) {
        for(short ss = 0; ss < m_stacks; ss++) {
            input >> atCell(tt, ss);
            m_latestPriority = std::max((cell)m_latestPriority, atCell(tt, ss));
        }
    }
}

void Instance::loadInstanceCV(std::ifstream& input, int dataFileFlag) {
//     std::cout << "Reading instance in Casserta/Voss format. " << std::endl;
    // Read in Casserta/Voss format
    m_stacks = dataFileFlag;
    int totalElements = 0;
    input >> totalElements;
    m_tiers = (totalElements / m_stacks) + 2; // Note: we add 2 extra layers as in the CV paper
    m_cells.resize(m_tiers * m_stacks, 0);
    for(short ss = 0; ss < m_stacks; ++ss) {
        short stackTiers = 0;
        input >> stackTiers;
        for(short tt = 0; tt < stackTiers; ++tt) {
            input >> atCell(tt, ss);
            m_latestPriority = std::max((cell)m_latestPriority, atCell(tt, ss));
        }
    }
}

void Instance::print() const {
    for(short tt = m_tiers - 1; tt >= 0; --tt) {
        for(short ss = 0; ss < m_stacks; ++ss) {
            if(atCell(tt,ss) > 0) {
                std::cout << std::setw(3) << atCell(tt, ss);
            } else {
                std::cout << std::setw(3) << " ";
            }
        }
        std::cout << std::endl;
    }
}

