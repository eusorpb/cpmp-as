/*
 * This code is Copyright (c) 2014 Kevin Tierney and is licensed under the
 * BSD-new license. See license.txt for details.
 */

#ifndef PM_INSTANCE
#define PM_INSTANCE

#include <fstream>
#include <vector>

typedef unsigned short ushort;
typedef short cell; // container group (priority) (or in ship stowage terms, the discharge port)

class Instance {
public:
    Instance(const char* instPath);

    inline ushort tiers() const { return m_tiers; }
    inline ushort stacks() const { return m_stacks; }
    inline cell latestPriority() const { return m_latestPriority; }
    inline const std::vector<cell>& cells() const { return m_cells; }

    void print() const;

    void loadInstanceBF(std::ifstream& input);
    void loadInstanceExpo(std::ifstream& input);
    void loadInstanceDario(std::ifstream& input);
    void loadInstanceCV(std::ifstream& input, int dataFileFlag);

    inline cell& atCell(const int& tier, const int& stack) {
        return m_cells[tier * m_stacks + stack];
    }
    
    inline const cell& atCell(const int& tier, const int& stack) const {
        return m_cells[tier * m_stacks + stack];
    }

private:
    short m_tiers;
    short m_stacks;
    cell m_latestPriority;
    // 1-dimensional array stored with format: [tier][stack]; The bottom of each stack is located at tier 0.
    std::vector<cell> m_cells; 
};


#endif

