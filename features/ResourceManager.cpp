/*
 * This code is Copyright (c) 2014 Kevin Tierney and is licensed under the
 * BSD-new license. See license.txt for details.
 */

#include "ResourceManager.h"

#include <cerrno>
#include <csignal>
#include <cstring>

#include <iostream>

ResourceManager* ResourceManager::s_resourceManager = NULL;
bool ResourceManager::s_cpuLimitNotReached = true;
time_t ResourceManager::s_initialTime = time (NULL);
time_t ResourceManager::s_maxWallClock = 298;

ResourceManager& ResourceManager::getResourceManager() {
    if(!s_resourceManager) {
        s_resourceManager = new ResourceManager();
        signal(SIGINT, ResourceManager::sigIntTermHandler);
        signal(SIGTERM, ResourceManager::sigIntTermHandler);
        signal(SIGXCPU, ResourceManager::sigxCpuSignalHandler);
    }
    return *s_resourceManager;
}

void ResourceManager::sigxCpuSignalHandler(int sig) {
    s_cpuLimitNotReached = false;
    // Continue to catch the signal, just so the program doesn't get killed by it
    signal(SIGXCPU, ResourceManager::sigxCpuSignalHandler);
}

void ResourceManager::sigIntTermHandler(int sig) {
    s_cpuLimitNotReached = false;
    // 2 = sigint
    // 15 = sigterm
    std::cout << "[INFO] Received ";
    if(sig == 2) {
        std::cout << "SIGINT";
    } else if(sig == 15) {
        std::cout << "SIGTERM";
    }
    std::cout << "! Terminating." << std::endl;
    // Continue to catch the signal, just so the program doesn't get killed by it
//     signal(SIGINT, ResourceManager::sigIntTermHandler);
//     signal(SIGTERM, ResourceManager::sigIntTermHandler);
}

ResourceManager::ResourceManager() 
    : m_memLimit(-1),
      m_memLimitSet(false),
      m_cpuSoftLimit(-1),
      m_cpuHardLimit(-1),
      m_cpuLimitSet(false) {
}

ResourceManager::~ResourceManager() {
}

void ResourceManager::setMemoryLimit(long long maxMemory) {
    // TODO Does the soft memory limit generate any signals?
    m_memLimit = maxMemory;
    struct rlimit rl;
    rl.rlim_cur = maxMemory;
    rl.rlim_max = maxMemory + 1;
    if(setrlimit(RLIMIT_AS, &rl) == -1) {
        std::cerr << "Error setting memory limit: " << strerror(errno) << std::endl;
//         throw ResourceSetException(errno, maxMemory);
    }
    m_memLimitSet = true;
}

void ResourceManager::setCpuLimit(double softLimit, double hardLimit) {
    signal(SIGXCPU, ResourceManager::sigxCpuSignalHandler);
    struct rlimit rl;
    rl.rlim_cur = softLimit;
    rl.rlim_max = hardLimit;
    if(setrlimit(RLIMIT_CPU, &rl) == -1) {
        std::cerr << "Error setting CPU limit: " << strerror(errno) << std::endl;
//         throw ResourceSetException(errno, softLimit, hardLimit);
    } 
    
}

long long ResourceManager::memoryLimit() const {
    return m_memLimit;
}

bool ResourceManager::memoryLimitSet() const {
    return m_memLimitSet;
}

double ResourceManager::cpuSoftLimit() const {
    return m_cpuSoftLimit;
}

double ResourceManager::cpuHardLimit() const {
    return m_cpuHardLimit;
}

bool ResourceManager::cpuLimitSet() const {
    return m_cpuLimitSet;
}

time_t ResourceManager::wallClockTime() {
    return time(NULL);
}

time_t ResourceManager::passedWallClockTime(){
  return time(NULL) - s_initialTime;
}
