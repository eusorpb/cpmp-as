# README #

This project contains code to generate machine learning features and instances for the container pre-marshalling problem. It is licensed using the BSD-new license.

## Features ##

### Building ###

The code compiles with g++-4.8 in Linux and OS X with no external dependencies. Simply type "make" on the command line in the directory with the source code.

### Windows support ###

This code probably works on Windows if the ResourceManager is removed from the code. We provide no guarantees at this time, but will try to make modifications for Windows if there is interest.

## Instance generation ##

Our instance generation tool attempts to recreate instances from the papers of Bortfeldt & Forster (2012) and Caserta and Voss (2009). Full citations follow.  We believe we generate instances in exactly the same way as Caserta and Voss.  However, in the case of the Bortfeldt & Forster instances, not all details are present in their paper. We follow all of the instructions they gave and do our best to make our instances look and feel like theirs to fill in the gaps.

## References ##

Please cite the following paper if you use our code in your work:
Tierney, K., Pacino, D., Voss, S.: Solving the pre-marshalling problem to optimality with A* and IDA*. Under revision at Flexible Services and Manufacturing (2016).

Other references relevant to this project:
Bortfeldt, A., Forster, F.: A tree search procedure for the container pre-marshalling problem. European Journal of Operational Research 217(3), 531–540 (2012)
Caserta, M., Voß, S.: A corridor method-based algorithm for the pre-marshalling problem. In: Giacobini, M., Brabazon, A., Cagnoni, S., Caro, G., Ekart, A., Esparcia-Alcazar, A., Farooq, M., Fink, A., Machado, P. (eds.) Applications of Evolutionary Computing. Lecture Notes in Computer Science, vol. 5484, pp. 788–797. Springer Berlin Heidelberg (2009)